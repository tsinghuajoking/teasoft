#!/usr/local/bin/python
# -*- coding: gbk -*-
#============================================================
# COLURL.PY                    -- by Dr. ZhuoQing 2021-05-13
#
# Usage: colurl title str
#        key, press right key
#
#        # : Change -[] into text
#
# Note: This command is used with csdntmp for collect url into
#       reference list.
#       
#============================================================

from head import *
import requests
from bs4 import BeautifulSoup

#------------------------------------------------------------
csdn_title = '写文章-CSDN博客'
titlestr = 'CSDN文章%D'

insertstr = ''

if len(sys.argv) > 1:
    titlestr = ''.join(sys.argv[1:])
else: titlestr = clipboard.paste()

#------------------------------------------------------------
if titlestr[0] == '#':
    titlestr = clipboard.paste()
    titlestr = titlestr.replace('- [ ]', '')
    titlestr = titlestr.replace('[**', '')
    titlestr = titlestr.replace('**](', ': ')
    titlestr = titlestr.replace(')', '\r\n')

    printf(titlestr)
    clipboard.copy(titlestr)
    printf('\a')
    exit()


#------------------------------------------------------------
headstr = ' - [ ] '

if titlestr.find('[]') >= 0:
    headstr = ' - [ ] '
    titlestr = titlestr.replace('[]', '')

if titlestr.find('#') >= 0:
    headstr = '#'
    titlestr = titlestr.replace('#', '')

if titlestr.find('*') >= 0:
    headstr = '* '
    titlestr = titlestr.replace('*', '')

#------------------------------------------------------------
def NoString(num):
    numstr = "零一二三四五六七八九十"

    if num >= 1 and num <= 10:
        return "%s"%numstr[num]
    elif num >= 11 and num < 20:
        return "十%c"%numstr[num-10]
    elif num >= 21 and num < 30:
        return "二十%c"%numstr[num-20]
    elif num >= 31 and num < 40:
        return "三十%c"%numstr[num-30]
    elif num >= 41 and num < 50:
        return "四十%c"%numstr[num-40]
    elif num >= 51 and num < 60:
        return "五十%c"%numstr[num-50]
    elif num >= 61 and num < 70:
        return "六十%c"%numstr[num-60]
    elif num >= 71 and num < 80:
        return "七十%c"%numstr[num-70]
    elif num >= 81 and num < 90:
        return "八十%c"%numstr[num-80]
    elif num >= 91 and num < 100:
        return "九十%c"%numstr[num-90]
    else: return "一"

#------------------------------------------------------------
clipboard.copy('')
printf('%s for url, Left button exit.\a'%titlestr)
linecount = 0

while True:
    key = tspread()
    if key[2] != 0: break

    if key[3] != 0:
        linecount = 0
        printf("Clear line count.\a")

    pastestr = clipboard.paste()

    if len(pastestr) > 0:
        clipboard.copy('')

        linecount += 1

        if headstr == '#':
            linehead = '%d. '%linecount
        else: linehead = headstr

        title = titlestr
        if title.find("%d") >= 0:
            title = title.replace("%d", '%d'%linecount)

        if title.find("%D") >= 0:
            title = title.replace("%D", '%s'%NoString(linecount))
        else:
            title = title.replace("第一", '第%s'%NoString(linecount))


        linestr = linehead + '[**%s**](%s)'%(title, pastestr)

        printf(linestr)
        printf('\a')
        insertstr = insertstr + linestr + '\r\n'


    time.sleep(.1)

#------------------------------------------------------------

printf('\a')

clipboard.copy(insertstr)
#printf(insertstr)

time.sleep(.5)

tspsendwindowkey(csdn_title, 'v', control=1)




#------------------------------------------------------------
#        END OF FILE : COLURL.PY
#============================================================
