#!/usr/local/bin/python
# -*- coding: gbk -*-
#============================================================
# INSII.PY                    -- by Dr. ZhuoQing 2020-09-30
#
# Note:
#============================================================

from head import *


pastestr = clipboard.paste().split('\r\n')
printf(pastestr)

#------------------------------------------------------------
insertstr = ''
lineflag = 0
for s in pastestr:

    s = s.rstrip('\r')

    if len(s) > 0:
        if len(insertstr) > 0:
            tspinsii('tspinsii("%s")'%insertstr)
        insertstr = s
        lineflag = 1
    else:
        insertstr = insertstr + '\\r\\n'


if len(insertstr) > 0 and lineflag:
    tspinsii('tspinsii("%s")'%insertstr)

clipboard.copy("")

#------------------------------------------------------------
#        END OF FILE : INSII.PY
#============================================================
