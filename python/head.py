#-------------------------------------------------------------
#!/usr/local/bin/python
# -*- coding: gbk -*-

import sys, os
sys.path.append(r'd:\python\teasoft')
STDFILE = open(r'd:\python\std.txt', 'a', 1)
sysstderr = sys.stderr
sysstdout = sys.stdout
sys.stderr = STDFILE
sys.stdout = STDFILE

from threading import Thread
import time, math, winsound, clipboard, random
from numpy import *

from tsmodule.tspdata   import *
from tsmodule.tspyt     import *
from tsmodule.tscmd     import *
from tsmodule.tsdopop   import *
from tsmodule.tsdraw    import *
import tsmodule.tsconfig
#-------------------------------------------------------------
