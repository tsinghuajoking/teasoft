<h1 align="center"> <center>TEASOFT教学软件</center> </h1>

<p align="center"><img src="https://img-blog.csdnimg.cn/06fa0741b4d44151a5953ad757113323.png" width="800" />

>[<font  face=黑体 color=purple size=4>简 介：</font>](#999000) 本文给出了教学软件TEASOFT基本介绍。
<br><font color=slateblue face=宋体><br>**``关键词``**：</font> **TEASOFT**，**教学软件**



<h1 align="center"> <center><font  face=黑体 color=slateblue size=6>§<u>01</u> <font color=red>教</font>学软件</font></center> </h1>

---


## 一、软件功能介绍


TEASOFT软件用于教学、网文编辑、MOOC课程录制以及Python编程软件环境。这个软件从2007年开始设计，功能逐渐丰富，现在仍然在不断建设当中。

+ [**CSDN上相关说明**](https://zhuoqing.blog.csdn.net/article/details/107465301) 



### ◎ 附件


1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
